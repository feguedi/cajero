#-------------------------------------------------
#
# Project created by QtCreator 2016-05-04T12:47:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Cajero
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += main.ui \
    billetes.ui \
    form_pago.ui \
    form_abonar.ui \
    form_espera.ui \
    form_recibo.ui \
    historico.ui \
    form_imprimiendo.ui

RESOURCES += \
    imagenes.qrc \
    fuentes.qrc

DISTFILES +=
