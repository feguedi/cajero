module Main (main) where

main :: IO ()
main = raices3

raices3 a b c
           |enum >= 0 = ((-b + sqrt(b ^ 2 - 4 * a * c)) / (2 * a),
                        (-b - sqrt(b ^ 2 - 4 * a * c)) / (2 * a))
             |otherwise=error"raices complejas"
            where
                enum = b ^ 2 - 4 * a * c
                raizEnum = sqrt enum
                deno = 2 * a