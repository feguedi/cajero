import System.IO
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    print f args

f :: [String] -> Integer
f x = 
    return x - 1
