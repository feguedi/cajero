module Main (main) where

main :: IO ()
main = juego2
    
juego2 :: IO ()
juego2 = do
    hSetBuffering stdout NoBuffering
    n <- randomRIO (1::Int, 100)
    putStrLn "Tienes que adivinar un numero entre 1 y 100"
    adivina2 n

adivina2 :: Int -> IO ()
adivina2 n = do
    putStr "Escribe un numero: "
    c <- getLine
    let x = read c
    case compare x n of
      LT -> do putStrLn " es bajo."
              adivina2 n
      GT -> do putStrLn " es alto."
              adivina2 n
      EQ -> do putStrLn " Exactamente"
