module Main (main) where


main :: IO ()
main = juego1

juego1 :: IO()
juego1 =
    do 
        putStrLn "Piensa un numero entre el 1 y el 100."
        adivina 1 100
        putStrLn "Fin del juego"

adivina :: Int -> Int -> IO()
adivina a b = do
    putStr("Es " ++ show conjetura ++ "? [mayor/menor/exacto]");
    s <- getLine
    case s of
        "mayor" -> adivina (conjetura + 1) b
        "menor" -> adivina a (conjetura - 1)
        "exacto" -> return()
        _        -> adivina a b
    where
        conjetura = (a + b) `div` 2
