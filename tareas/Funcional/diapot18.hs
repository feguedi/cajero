sp_help item@(Item cur_loc cur_link _) wq vis
  | cur_length > limit	   	-- Beyond limit
  = sp wq vis
  | Just vis_link <- lookupVisited vis cur_loc
  = 	-- Already visited; update the visited
		-- map if cur_link is better
    if cur_length >= linkLength vis_link then
		-- Current link is no better
	     sp wq vis
    else	
		-- Current link is better
	     emit vis item ++ sp wq vis'

  | otherwise	-- Not visited yet
  = emit vis item ++ sp wq' vis'
  where
	vis’ = ...
	wq   = ...
