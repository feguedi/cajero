-- Función error
raizCuadrada n = if n >= 0 then sqrt(n)
else error "Raíz compleja"


-- Función en partes
absoluto :: Integer -> Integer
	| x >= 0 = x
	| x < 0 = -x

signo :: Integer -> Integer
signo x
	| x > 0 = 1
	| x == 0 = 0
	| otherwise = -1


-- Expresiones condicionales
--if expresionBool then expresionSi else expresionNo
if 5 > 2 then 10.0 else (10.0 / 0.0)

-- Definiciones locales
raices a b c
	| b ^ 2 - 4 * a * c > 0 = ((-b + sqrt(b ^ 2 - 4 * a * c)) / (2 * a),
								(-b - sqrt(b ^ 2 - 4 * a * c)) / (2 * a)
	| otherwise = error "Raices complejas"

raices2 a b c
	| enum >= 0 = ((-b + sqrt(b ^ 2 - 4 * a * c)) / (2 * a),
					(-b - sqrt(b ^ 2 - 4 * a * c)) / (2 * a)
	| otherwise = error "Raices complejas"
	where
		enum = b ^ 2 - 4 * a * c
		raizEnum = sqrt enum
		deno = 2 * a


-- Salida
Prueba = do
	putStrLn "Esto es"
	putStrLn " una prueba"


-- Entrada
saludo = do
	putStrLn "Tu nombre: "
	nom <- getLine
	putStrLn ("Hola " ++ nom)
