module Main (raices) where
raices a b c
    |b ^ 2 - 4 * a * c > 0 = ((-b + sqrt(b ^ 2 - 4 * a * c)) / (2 * a),
                                (-b - sqrt(b ^ 2 - 4 * a * c)) / (2 * a))
    |otherwise error "raices complejas"