# -*- coding: utf-8 -*-
from threading import Semaphore, Lock, Thread
from time import sleep
from random import randrange

tam = 10
linea = 3
cadena = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]
coloco = Semaphore(value=10)
mutex = Lock()
permiso = [Semaphore(value=0), Semaphore(value=0), Semaphore(value=0)]


class Colocar(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            try:
                i, n, l = 0, randrange(1, 3), randrange(1, 3)
                print("Produciendo tipo", n)
                sleep(1)
                coloco.acquire(True)
                while cadena[l][i] != 0:
                    i += 1
                cadena[l][i] = n
                print("Coloco un producto", n, "en la posición", i, "linea de produccion", (l + 1))
                print(cadena[l])
                permiso[n - 1].release()

            except KeyboardInterrupt:
                print("Se interrumpió el colocador")


class Empaquetar(Thread):
    def __init__(self, tipo, n):
        Thread.__init__(self)
        self.tipo = tipo
        self.n = n

    def run(self):
        i, l, nemp = 0, randrange(1, 3), 0
        print("Iniciando Empaquetador")
        while True:
            try:
                permiso[self.tipo - 1].acquire()
                while cadena[l][i] != self.tipo:
                    i += 1
                    if i > 9:
                        l = randrange(1, 3)
                        i = 0
                sleep(1)
                print("Robot de tipo", self.tipo,
                      "empaquetando el producto de la posición", i, "linea de produccion", l + 1)
                cadena[l][i] = 0
                coloco.release()
                sleep(2)
                mutex.acquire()
                nemp += 1
                print("Aumenta el número de empaquetados a:", nemp)
                mutex.release()

            except KeyboardInterrupt:
                print("Se interrupió el empaquetamiento")


if __name__ == '__main__':
    print("El tamaño de la cadena será", tam)
    print("Hay", linea, "líneas de producción")
    colocador = Colocar()
    hilos = [Empaquetar(1, randrange(1, stop=3)), Empaquetar(2, randrange(1, stop=3)),
             Empaquetar(3, randrange(1, stop=3))]
    colocador.start()
    for h in hilos:
        h.start()
