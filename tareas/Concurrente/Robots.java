/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robots;

import java.util.concurrent.locks.*;
import java.util.concurrent.Semaphore;
import java.util.*;

/**
 *
 * @author feguedi
 */
public class Robots {

    static int tam = 10;
    static int [] cadena = new int [tam];
    static Semaphore coloco=new Semaphore(tam, true);
    static Semaphore [] puedoemp =  {new Semaphore(0), new Semaphore(0), new Semaphore(0)};
    static int nemp = 0; // necesita un mutex
    static Semaphore mutex = new Semaphore(1, true);
    static Random r = new Random();
    
    public static class Colocador extends Thread{
        public void run(){
            int i, n;
            while(true){
                try {
                    n = r.nextInt(3)+1;
                    System.out.println("Produciendo tipo "+ n);
                    sleep(r.nextInt(1000));
                    coloco.acquire();
                    i=0;
                    while(cadena[i] != 0) i++; //cuando encuentra un sitio vacío, sale del ciclo y puede colocar
                    cadena[i]  = n;
                    System.out.println("Coloco un producto "+ n + " en la posición "+ i +"\n"+ Arrays.toString(cadena));
                    puedoemp[n-1].release();
                } catch (InterruptedException e) {e.printStackTrace(); }
            }    
        }
    }
    
    public static class Empaquetador extends Thread {
        private int tipo;
        public Empaquetador (int tipo){
            this.tipo = tipo;
        }

        public void run(){
            int i;
            while(true){
                try {
                    puedoemp[tipo-1].acquire();
                     i =0;
                    while(cadena[i] != tipo) i++;
                    sleep(r.nextInt(1000));
                    System.out.println("Robot de tipo "+ tipo + " empaquetando el producto de la posición "+ i);
                    cadena[i] = 0; //deja el hueco
                    coloco.release();
                    sleep(2000);
                    mutex.acquire();  //aumentar nemp
                    nemp++;
                    System.out.println("Aumenta el número de empaquetados: "+ nemp);
                    mutex.release(); //     
                } catch (InterruptedException e) {e.printStackTrace();}
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("El tamaño de la cadena será de " + tam);
        Empaquetador  empaquetador1 = new Empaquetador(1);
        Empaquetador  empaquetador2 = new Empaquetador(2);
        Empaquetador  empaquetador3 = new Empaquetador(3);
        Colocador colocador = new Colocador();
        colocador.start();
        empaquetador1.start();
        empaquetador2.start();
        empaquetador3.start();
    }
}

/**
 * 
 * TAREA
 * 3 robots que empaquetan sobre 2 líneas
 * 
 */