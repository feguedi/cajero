# -*- coding: utf-8 -*-
import threading
from threading import Semaphore
import random
import time

# Del ejemplo de los robots colocador y empaquetar,
# hacer lo mismo pero considerando 3 líneas de producción.

tam = 10
cadena = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
coloco = Semaphore(tam)
puedoEmp = [Semaphore(0), Semaphore(0), Semaphore(0)]
nemp = 0
mutex = Semaphore(1)


class Colocar(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print("Corriendo colocador")

        while True:
            print("Entrando al colocador")
            try:
                print("Colocando")
                n = random.randrange(1, 4) + 1
                l = random.randrange(1, 4)
                print("Produciendo tipo", n)
                time.sleep(1)
                coloco.acquire()
                i = 0
                while cadena[l][i] != 0:
                    i += 1
                cadena[l][i] = n
                print("Coloco un producto", n, "en la posición", i, "línea de producción", l + 1)
                print(cadena[l])
                puedoEmp[n - 1].release()

            except KeyboardInterrupt:
                print("Se interrumpió el colocador")


class Empaquetar(threading.Thread):
    def __init__(self, id):
        threading.Thread.__init__(self)
        self.id = id
        self.nemp = nemp

    def run(self):
        print("Corriendo empaquetador")
        while True:
            print("Entrando al empaquetador")
            try:
                print("A punto de ejecutar el empaquetador")
                puedoEmp[self.id - 1].acquire(True)
                print("¿Se hace algo?")
                i = 0
                l = random.randrange(1, 4)
                while cadena[l][i] != self.id:
                    print("Entrando infinitamente en el ciclo While del empaquetador", self.id)
                    i += 1
                    if i > 9:
                        l = 0
                        i = 0
                time.sleep(1)
                print("Robot tipo", self.id, "empaquetando el producto en la posición", i,
                      "línea de producción", l + 1)
                cadena[l][i] = 0
                coloco.release()
                time.sleep(1.5)
                mutex.acquire()
                self.nemp += 1
                print("Aumenta el número de empaquetados:", self.nemp)
                mutex.release()

            except KeyboardInterrupt:
                print("Se interrumpió el empaquetador")


if __name__ == '__main__':
    empaquetador = [Empaquetar(1), Empaquetar(2),
                    Empaquetar(3)]

    colocador = threading.Thread(target=Colocar, name='Colocador')
    print("El tamaño de la cadena será de", tam, "sobre", 3, "líneas de producción")
    colocador.start()
    for emp in empaquetador:
        emp.start()
