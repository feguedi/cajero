# -*- coding: utf-8 -*-
import threading
import time
__author__ = 'nger'

cond = threading.Condition()

mesa = []


class Cliente (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            cond.acquire()
            cond.wait()
            mesa.pop()
            cond.notify()
            cond.release()


class Cocinero(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            cond.acquire()
            if len(mesa) != 0:
                cond.wait()
            time.sleep(2)
            mesa.append("Pastel de frutas")
            cond.notify()
            cond.release()

if __name__ == '__main__':
    print("El bar")
    cliente = Cliente()
    cocinero = Cocinero()
    cliente.start()
    cocinero.start()
    print(mesa)
