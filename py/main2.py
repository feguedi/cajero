# -*- coding: utf-8 -*-
import sys

# from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QDesktopWidget
from PyQt5.QtCore import Qt
import main
import historico
import form_pago
import form_abonar
import billetes
import form_recibo2
import random

cobro_kwh = 0.809
#                            Dirección,        Deuda Anterior,         Deuda Recibo
usuarios = {
    "Hernández, José": [['53, #14, Lomas de Casablanca', '5R0P87', 145.00, 320.00],
                        [random.randrange(145, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)],
                        [random.randrange(50, 450, 3)]
                        ],
    "Alamilla, Teresa": [['Av. Zaragoza, #1000, San José de los Olvera', '3W2V45', 0.0, 450.0],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)]
                         ],
    "Beltrán, Axel": [['Prol. Constituyentes, #3480, Loma Dorada', '7X9Q98', 15.0, 153.0],
                      [[random.randrange(15, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)],
                       [random.randrange(50, 450, 3)]]
                      ],
    "Coronel, Alberto": [['Bernardo Quintana, #123, Las Cruces', '1A2B23', 109.0, 225.0],
                         [random.randrange(109, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)],
                         [random.randrange(50, 450, 3)]
                         ],
    "Piña, Beatriz": [['Av. Tecnológico, #13, Centro', '4B0C33', 0.0, 130.0],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)],
                      [random.randrange(50, 450, 3)]
                      ],
    "García, Elizabeth": [['Av. 5 de febrero, #834, Juriquilla', '2Y7X65', 78.0, 345.0],
                          [random.randrange(78, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)],
                          [random.randrange(50, 450, 3)]
                          ],
}


class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.showMinimized()
        self.menu = main.UiForm()
        self.pagar = form_pago.UiForm()
        self.hist = historico.UiForm()
        self.abonar = form_abonar.UiForm()
        self.din = billetes.UiForm()
        self.imprime = form_recibo2.UiForm()
        self.setWindowTitle("CFEMático")
        self.imprimir()
        # global abonado
        self.cambio = 0
        self.abonado = 0
        self.deuda_total = 0
        self.seleccion = ""

    def princ(self):
        screen = QDesktopWidget().availableGeometry()
        y_pos = (screen.height() / 2) - 340
        x_pos = (screen.width() / 2) - 512
        self.menu.move(x_pos, y_pos)
        self.menu.btn_lector.clicked['bool'].connect(self.selec)
        # self.menu.btn_lector.clicked.connect(self.menu.progressBar.setVa('value', barra + 3))
        # self.menu.btn_lector.released.connect(self.menu.progressBar.setProperty('value', 0))
        self.menu.show()
        self.close()

    # def suma_barrita(self):
    #     for i in range(1, 33):
    #         return i * 3

    def selec(self):
        self.seleccion = str(self.menu.bx_clientes.currentText())
        self.deuda_total = usuarios.get(self.seleccion)[0][2] + usuarios.get(self.seleccion)[0][3]
        self.menu.close()
        print(self.seleccion)
        self.pago()

    def pago(self):
        self.setWindowTitle("CFEMático")
        screen = QDesktopWidget().availableGeometry()
        y_pos = (screen.height() / 2) - 340
        x_pos = (screen.width() / 2) - 512
        self.pagar.move(x_pos, y_pos)
        self.pagar.lbl_m_cliente.setText(self.seleccion)
        self.pagar.lbl_m_domicilio.setText(usuarios.get(self.seleccion)[0][0])
        self.pagar.lbl_m_medidor.setText(usuarios.get(self.seleccion)[0][1])
        self.pagar.lbl_m_ad_ant.setText(str(usuarios.get(self.seleccion)[0][2]))
        self.pagar.lbl_m_ad_rec.setText(str(usuarios.get(self.seleccion)[0][3]))
        self.pagar.lbl_m_ad_tot.setText(str(self.deuda_total))
        self.pagar.show()
        self.pagar.btn_pagar.clicked['bool'].connect(self.abono)
        self.pagar.btn_historico.clicked['bool'].connect(self.historial)
        self.pagar.btn_cancelar.clicked['bool'].connect(exit)

    def historial(self):
        self.pagar.close()
        self.setWindowTitle("CFEMático")
        screen = QDesktopWidget().availableGeometry()
        y_pos = (screen.height() / 2) - 340
        x_pos = (screen.width() / 2) - 512
        self.hist.move(x_pos, y_pos)
        self.hist.lbl_kwh_1.setText("{0}".format(str(usuarios.get(self.seleccion)[1][0])))
        kwh = usuarios.get(self.seleccion)[1][0] * cobro_kwh
        self.hist.lbl_imp_1.setText("$ {0}".format(str(kwh)[0:6]))
        self.hist.lbl_pag_1.setText("$ {0}".format(str(
            float(usuarios.get(self.seleccion)[1][0] * cobro_kwh) - float(usuarios.get(self.seleccion)[0][2]))[0:6]))
        self.hist.lbl_pen_1.setText("{0}".format(str(usuarios.get(self.seleccion)[0][2]))[0:5])
        self.hist.lbl_pen_2.setText("{0}".format(""))
        self.hist.lbl_kwh_2.setText("{0}".format(str(usuarios.get(self.seleccion)[1][0])))
        self.hist.lbl_imp_2.setText("$ {0}".format(str(usuarios.get(self.seleccion)[1][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_2.setText("$ {0}".format(str(usuarios.get(self.seleccion)[1][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_3.setText("{0}".format(""))
        self.hist.lbl_pag_3.setText("$ {0}".format(str(usuarios.get(self.seleccion)[2][0] * cobro_kwh)[0:6]))
        self.hist.lbl_kwh_3.setText("{0}".format(str(usuarios.get(self.seleccion)[2][0])))
        self.hist.lbl_imp_3.setText("$ {0}".format(str(usuarios.get(self.seleccion)[2][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_4.setText("{0}".format(""))
        self.hist.lbl_imp_4.setText("$ {0}".format(str(usuarios.get(self.seleccion)[3][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_4.setText("$ {0}".format(str(usuarios.get(self.seleccion)[3][0] * cobro_kwh)[0:6]))
        self.hist.lbl_kwh_4.setText("{0}".format(str(usuarios.get(self.seleccion)[3][0])))
        self.hist.lbl_pen_5.setText("{0}".format(""))
        self.hist.lbl_imp_5.setText("$ {0}".format(str(usuarios.get(self.seleccion)[4][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_5.setText("$ {0}".format(str(usuarios.get(self.seleccion)[4][0] * cobro_kwh)[0:6]))
        self.hist.lbl_kwh_5.setText("{0}".format(str(usuarios.get(self.seleccion)[4][0])))
        self.hist.lbl_pen_6.setText("{0}".format(""))
        self.hist.lbl_kwh_6.setText("{0}".format(str(usuarios.get(self.seleccion)[5][0])))
        self.hist.lbl_pag_6.setText("$ {0}".format(str(usuarios.get(self.seleccion)[5][0] * cobro_kwh)[0:6]))
        self.hist.lbl_imp_6.setText("$ {0}".format(str(usuarios.get(self.seleccion)[5][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_7.setText("{0}".format(""))
        self.hist.lbl_imp_7.setText("$ {0}".format(str(usuarios.get(self.seleccion)[6][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_7.setText("$ {0}".format(str(usuarios.get(self.seleccion)[6][0] * cobro_kwh)[0:6]))
        self.hist.lbl_kwh_7.setText("{0}".format(str(usuarios.get(self.seleccion)[6][0])))
        self.hist.lbl_imp_8.setText("$ {0}".format(str(usuarios.get(self.seleccion)[7][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_8.setText("$ {0}".format(str(usuarios.get(self.seleccion)[7][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_8.setText("{0}".format(""))
        self.hist.lbl_imp_9.setText("$ {0}".format(str(usuarios.get(self.seleccion)[8][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_9.setText("$ {0}".format(str(usuarios.get(self.seleccion)[8][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_9.setText("{0}".format(""))
        self.hist.lbl_pen_10.setText("{0}".format(""))
        self.hist.lbl_pag_10.setText("$ {0}".format(str(usuarios.get(self.seleccion)[9][0] * cobro_kwh)[0:6]))
        self.hist.lbl_imp_10.setText("$ {0}".format(str(usuarios.get(self.seleccion)[9][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_11.setText("{0}".format(""))
        self.hist.lbl_imp_11.setText("$ {0}".format(str(usuarios.get(self.seleccion)[10][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_11.setText("$ {0}".format(str(usuarios.get(self.seleccion)[10][0] * cobro_kwh)[0:6]))
        self.hist.lbl_imp_12.setText("$ {0}".format(str(usuarios.get(self.seleccion)[11][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_12.setText("$ {0}".format(str(usuarios.get(self.seleccion)[11][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_12.setText("{0}".format(""))
        self.hist.lbl_imp_13.setText("$ {0}".format(str(usuarios.get(self.seleccion)[12][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_13.setText("{0}".format(""))
        self.hist.lbl_pag_13.setText("$ {0}".format(str(usuarios.get(self.seleccion)[12][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_14.setText("{0}".format(""))
        self.hist.lbl_pag_14.setText("$ {0}".format(str(usuarios.get(self.seleccion)[13][0] * cobro_kwh)[0:6]))
        self.hist.lbl_imp_14.setText("$ {0}".format(str(usuarios.get(self.seleccion)[13][0] * cobro_kwh)[0:6]))
        self.hist.lbl_imp_15.setText("$ {0}".format(str(usuarios.get(self.seleccion)[14][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_15.setText("$ {0}".format(str(usuarios.get(self.seleccion)[14][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_15.setText("{0}".format(""))
        self.hist.lbl_imp_16.setText("$ {0}".format(str(usuarios.get(self.seleccion)[15][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_16.setText("$ {0}".format(str(usuarios.get(self.seleccion)[15][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_16.setText("{0}".format(""))
        self.hist.lbl_imp_17.setText("$ {0}".format(str(usuarios.get(self.seleccion)[16][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_17.setText("$ {0}".format(str(usuarios.get(self.seleccion)[16][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_17.setText("{0}".format(""))
        self.hist.lbl_imp_18.setText("$ {0}".format(str(usuarios.get(self.seleccion)[17][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pag_18.setText("$ {0}".format(str(usuarios.get(self.seleccion)[17][0] * cobro_kwh)[0:6]))
        self.hist.lbl_pen_18.setText("{0}".format(""))
        self.hist.lbl_kwh_8.setText("{0}".format(str(usuarios.get(self.seleccion)[7][0])))
        self.hist.lbl_kwh_9.setText("{0}".format(str(usuarios.get(self.seleccion)[8][0])))
        self.hist.lbl_kwh_10.setText("{0}".format(str(usuarios.get(self.seleccion)[9][0])))
        self.hist.lbl_kwh_11.setText("{0}".format(str(usuarios.get(self.seleccion)[10][0])))
        self.hist.lbl_kwh_12.setText("{0}".format(str(usuarios.get(self.seleccion)[11][0])))
        self.hist.lbl_kwh_13.setText("{0}".format(str(usuarios.get(self.seleccion)[12][0])))
        self.hist.lbl_kwh_14.setText("{0}".format(str(usuarios.get(self.seleccion)[13][0])))
        self.hist.lbl_kwh_15.setText("{0}".format(str(usuarios.get(self.seleccion)[14][0])))
        self.hist.lbl_kwh_16.setText("{0}".format(str(usuarios.get(self.seleccion)[15][0])))
        self.hist.lbl_kwh_17.setText("{0}".format(str(usuarios.get(self.seleccion)[16][0])))
        self.hist.lbl_kwh_18.setText("{0}".format(str(usuarios.get(self.seleccion)[17][0])))
        self.hist.btn_ok.clicked['bool'].connect(self.hist.close and self.pago)
        self.hist.show()

    def abono(self):
        self.pagar.close()
        self.din.setWindowFlags(Qt.FramelessWindowHint)
        self.din.setAttribute(Qt.WA_TranslucentBackground)
        screen = QDesktopWidget().availableGeometry()
        # y_pos = (screen.height() / 2) + 340
        x_pos = (screen.width() / 2) - 375
        self.din.move(x_pos, 0)
        self.abonar.btn_cancelar.clicked['bool'].connect(self.abonar.close)
        self.abonar.btn_abonar.clicked['bool'].connect(self.abonar.close)
        screen = QDesktopWidget().availableGeometry()
        y_pos = screen.height() - 384
        x_pos = (screen.width() / 2) - 512
        self.abonar.move(x_pos, y_pos)

        self.abonar.lbl_m_cliente.setText(self.seleccion)
        self.abonar.lbl_m_adeudo.setText("$ {0}".format(str(self.deuda_total)))
        self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
        self.abonar.lbl_m_cambio.setText("$ {0}".format(str(self.cambio)))

        self.din.btn_1.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_10.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_100.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_2.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_20.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_200.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_5.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_50.clicked['bool'].connect(self.agregar_dinero)
        self.din.btn_500.clicked['bool'].connect(self.agregar_dinero)

        # self.abonar.btn_abonar.clicked['bool'].connect(self.imprimir)
        self.abonar.btn_abonar.clicked['bool'].connect(self.imprimir)
        self.abonar.btn_cancelar.clicked['bool'].connect(exit)

        self.abonar.show()
        self.din.show()
        self.close()

    # def espera(self):
    #     import time
    #     while True:
    #         import form_espera
    #         self.esperar = form_espera.UiForm()
    #         screen = QDesktopWidget().availableGeometry()
    #         y_pos = (screen.height() / 2) - 384
    #         x_pos = (screen.width() / 2) - 512
    #         self.esperar.move(x_pos, y_pos)
    #         self.esperar.show()
    #     time.sleep(3)

    def imprimir(self):
        self.imprime.show()
        import time
        self.abonar.hide()
        self.din.hide()
        self.setWindowTitle("CFEMático")
        screen = QDesktopWidget().availableGeometry()
        y_pos = (screen.height() / 2) - 340
        x_pos = (screen.width() / 2) - 512
        self.imprime.move(x_pos, y_pos)
        time.sleep(5)
        exit()

    def agregar_dinero(self):
        sender = self.sender()
        print(sender.objectName())
        if sender.objectName() == 'btn_1':
            self.abonado += 1
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_2':
            self.abonado += 2
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_5':
            self.abonado += 5
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_10':
            self.abonado += 10
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_20':
            self.abonado += 20
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_50':
            self.abonado += 50
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_100':
            self.abonado += 100
            print(self.abonado)

            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
        elif sender.objectName() == 'btn_200':
            self.abonado += 200
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        elif sender.objectName() == 'btn_500':
            self.abonado += 500
            print(self.abonado)
            self.abonar.lbl_m_depositado.setText("$ {0}".format(str(self.abonado)))
            print("Depositado: {0}".format(self.abonar.lbl_m_depositado.text()))

        if self.abonado > self.deuda_total:
            self.dar_cambio()

    def dar_cambio(self):
        print("Dando cambio")
        self.cambio = self.abonado - self.deuda_total
        self.abonar.lbl_m_cambio.setText("$ {0}".format(str(self.cambio)))

        print("Cambio: {0}".format(self.cambio))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Main()
    sys.exit(app.exec_())
