# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../form_espera.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QApplication


class UiForm(QWidget):
    def __init__(self):
        super().__init__()

        self.setup_ui(self)

    def setup_ui(self, Form):
        Form.setObjectName("Form")
        Form.resize(1024, 768)
        Form.setWindowTitle("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../images/logo.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        Form.setLayoutDirection(QtCore.Qt.LeftToRight)
        Form.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.frm_arriba = QtWidgets.QFrame(Form)
        self.frm_arriba.setEnabled(True)
        self.frm_arriba.setGeometry(QtCore.QRect(0, 0, 1024, 91))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frm_arriba.sizePolicy().hasHeightForWidth())
        self.frm_arriba.setSizePolicy(sizePolicy)
        self.frm_arriba.setMinimumSize(QtCore.QSize(0, 80))
        self.frm_arriba.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.frm_arriba.setStyleSheet("background-color: rgb(0, 183, 72);")
        self.frm_arriba.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frm_arriba.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frm_arriba.setObjectName("frm_arriba")
        self.lbl_logo_matico = QtWidgets.QLabel(self.frm_arriba)
        self.lbl_logo_matico.setGeometry(QtCore.QRect(10, 3, 291, 85))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_logo_matico.sizePolicy().hasHeightForWidth())
        self.lbl_logo_matico.setSizePolicy(sizePolicy)
        self.lbl_logo_matico.setStyleSheet("QLabel{\n"
                                           "background-color: transparent;}")
        self.lbl_logo_matico.setText("")
        self.lbl_logo_matico.setPixmap(QtGui.QPixmap("../images/CFEmatico.svg"))
        self.lbl_logo_matico.setScaledContents(True)
        # self.lbl_logo_matico.setFlat(True)
        self.lbl_logo_matico.setObjectName("lbl_logo_matico")
        self.lbl_logo_cfe = QtWidgets.QLabel(self.frm_arriba)
        self.lbl_logo_cfe.setGeometry(QtCore.QRect(720, 3, 291, 85))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_logo_cfe.sizePolicy().hasHeightForWidth())
        self.lbl_logo_cfe.setSizePolicy(sizePolicy)
        self.lbl_logo_cfe.setStyleSheet("background-color: transparent;")
        self.lbl_logo_cfe.setText("")
        self.lbl_logo_cfe.setPixmap(QtGui.QPixmap("../images/logo.svg"))
        # self.lbl_logo_cfe.setIconSize(QtCore.QSize(150, 150))
        self.lbl_logo_cfe.setScaledContents(True)
        # self.lbl_logo_cfe.setFlat(True)
        self.lbl_logo_cfe.setObjectName("lbl_logo_cfe")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(0, 290, 1024, 161))
        font = QtGui.QFont()
        font.setFamily("Helvetica Neue")
        font.setPointSize(40)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.label.setObjectName("label")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(90, 580, 851, 23))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 183, 72))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 183, 72))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(145, 141, 126))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, brush)
        self.progressBar.setPalette(palette)
        self.progressBar.setProperty("value", 60)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        self.show()

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("Form", "Espere un momento por favor"))
        Form.setWindowTitle(_translate("Form", "CFEmático"))


import imagenes_rc

if __name__ == '__main__':
    app = QApplication(sys.argv)
    x = ui_form()
    sys.exit(app.exec_())
