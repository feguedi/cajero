# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../form_recibo.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QApplication


class UiForm(QWidget):
    def __init__(self):
        super().__init__()

        self.setup_ui(self)

    def setup_ui(self, Form):
        Form.setObjectName("Form")
        Form.resize(1024, 768)
        Form.setMinimumSize(QtCore.QSize(1024, 768))
        Form.setMaximumSize(QtCore.QSize(1024, 768))
        Form.setWindowTitle("")
        Form.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lbl_msj = QtWidgets.QLabel(Form)
        self.lbl_msj.setGeometry(QtCore.QRect(0, 280, 1024, 161))
        font = QtGui.QFont()
        font.setFamily("Helvetica Neue")
        font.setPointSize(40)
        self.lbl_msj.setFont(font)
        self.lbl_msj.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_msj.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.lbl_msj.setObjectName("lbl_msj")
        self.lbl_img = QtWidgets.QLabel(Form)
        self.lbl_img.setGeometry(QtCore.QRect(450, 530, 121, 161))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_img.sizePolicy().hasHeightForWidth())
        self.lbl_img.setSizePolicy(sizePolicy)
        self.lbl_img.setText("")
        self.lbl_img.setPixmap(QtGui.QPixmap(":/espera/images/recibo-2.svg"))
        self.lbl_img.setScaledContents(True)
        self.lbl_img.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_img.setObjectName("lbl_img")
        self.frm_arriba = QtWidgets.QFrame(Form)
        self.frm_arriba.setEnabled(True)
        self.frm_arriba.setGeometry(QtCore.QRect(0, 0, 1024, 91))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frm_arriba.sizePolicy().hasHeightForWidth())
        self.frm_arriba.setSizePolicy(sizePolicy)
        self.frm_arriba.setMinimumSize(QtCore.QSize(0, 80))
        self.frm_arriba.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.frm_arriba.setStyleSheet("background-color: rgb(0, 183, 72);")
        self.frm_arriba.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frm_arriba.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frm_arriba.setObjectName("frm_arriba")
        self.lbl_logo_matico = QtWidgets.QLabel(self.frm_arriba)
        self.lbl_logo_matico.setGeometry(QtCore.QRect(100, 3, 140, 85))
        self.lbl_logo_matico.setText("")
        self.lbl_logo_matico.setPixmap(QtGui.QPixmap(":/encabezado/images/CFEmatico.svg"))
        self.lbl_logo_matico.setScaledContents(True)
        self.lbl_logo_matico.setObjectName("lbl_logo_matico")
        self.lbl_logo_cfe = QtWidgets.QLabel(self.frm_arriba)
        self.lbl_logo_cfe.setGeometry(QtCore.QRect(790, 8, 150, 75))
        self.lbl_logo_cfe.setText("")
        self.lbl_logo_cfe.setPixmap(QtGui.QPixmap(":/encabezado/images/logo.svg"))
        self.lbl_logo_cfe.setScaledContents(True)
        self.lbl_logo_cfe.setObjectName("lbl_logo_cfe")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        self.lbl_msj.setText(_translate("Form", "Un momento por favor...\n"
                                                "estamos imprimiendo su comprobante"))


import imagenes_rc

if __name__ == '__main__':
    app = QApplication(sys.argv)
    x = UiForm()
    x.show()
    sys.exit(app.exec_())
