# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../form_imprimiendo.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget, QApplication


class UiForm(QWidget):
    def __init__(self):
        super().__init__()

        self.setup_ui(self)

    def setup_ui(self, frm_imprimir):
        frm_imprimir.setObjectName("frm_imprimir")
        frm_imprimir.resize(1024, 768)
        self.gridLayout = QtWidgets.QGridLayout(frm_imprimir)
        self.gridLayout.setContentsMargins(0, -1, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.lbl_imagen = QtWidgets.QLabel(frm_imprimir)
        self.lbl_imagen.setMinimumSize(QtCore.QSize(200, 275))
        self.lbl_imagen.setMaximumSize(QtCore.QSize(200, 275))
        self.lbl_imagen.setText("")
        self.lbl_imagen.setPixmap(QtGui.QPixmap(":espera/images/recibo-2.svg"))
        self.lbl_imagen.setScaledContents(True)
        self.lbl_imagen.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_imagen.setObjectName("lbl_imagen")
        self.gridLayout.addWidget(self.lbl_imagen, 3, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 3, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 3, 2, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 2, 1, 1, 1)
        self.lbl_mensaje = QtWidgets.QLabel(frm_imprimir)
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(55)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.lbl_mensaje.setFont(font)
        self.lbl_mensaje.setStyleSheet("QLabel {\n"
                                       "font: 25 55pt \"Roboto\";\n"
                                       "\n"
                                       "color: black;\n"
                                       "}")
        self.lbl_mensaje.setText("")
        self.lbl_mensaje.setPixmap(QtGui.QPixmap(":espera/images/mensaje.svg"))
        self.lbl_mensaje.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.lbl_mensaje.setObjectName("lbl_mensaje")
        self.gridLayout.addWidget(self.lbl_mensaje, 1, 0, 1, 3)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout.addItem(spacerItem4, 4, 0, 1, 3)

        self.retranslateUi(frm_imprimir)
        QtCore.QMetaObject.connectSlotsByName(frm_imprimir)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

    def retranslateUi(self, frm_imprimir):
        _translate = QtCore.QCoreApplication.translate
        frm_imprimir.setWindowTitle(_translate("frm_imprimir", "Form"))

import imagenes_rc


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    x = UiForm()
    x.show()
    sys.exit(app.exec_())
